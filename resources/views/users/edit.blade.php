@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Información personal</h1></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ route('usuario.update',auth()->id() ) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault01">Nombre</label>
                          <input type="text" name="name" class="form-control" id="validationDefault01" value="{{auth()->user()->name}}" required>
                          </div>
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault02">Apellido Paterno</label>
                            <input type="text" name="apellidopat"class="form-control" id="validationDefault02" value="{{auth()->user()->apellidopat}}" required>
                          </div>
                        </div>

                        <div class="form-row">
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault01">Apellido Materno</label>
                          <input type="text" name="apellidomat" class="form-control" id="validationDefault01" value="{{auth()->user()->apellidomat}}" required>
                          </div>
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault02">Correo electrónico</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="validationDefault02" value="{{auth()->user()->email}}" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                          </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                              <label for="validationDefault01">CURP</label>
                            <input type="text" name="curp" class="form-control @error('curp') is-invalid @enderror" id="validationDefault01" value="{{auth()->user()->curp}}" required>
                                @error('curp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>El CURP ya existe.</strong>
                                </span>
                            @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                              <label for="validationDefault02">Telefono</label>
                              <input type="tel" name="telefono"class="form-control" id="validationDefault02" value="{{auth()->user()->telefono}}" required>
                            </div>
                          </div>
                        
                        <div class="form-group">
                          <div class="form-check">
                            
                          </div>
                        </div>
                        <button class="btn btn-success btn-lg btn-block">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
