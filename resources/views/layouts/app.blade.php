<style>
    .imagen1{
    width: 250px;
    height: px;
}
</style>
<!DOCTYPE doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
            <meta content="width=device-width, initial-scale=1" name="viewport">
                <!-- CSRF Token -->
                <meta content="{{ csrf_token() }}" name="csrf-token">
                    <title>
                        {{ config('app.name', 'Laravel') }}
                    </title>
                    <!-- Scripts -->
                    <script defer="" src="{{ asset('js/app.js') }}">
                    </script>
                    <!-- Fonts -->
                    <link href="//fonts.gstatic.com" rel="dns-prefetch">
                        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
                            <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
                                <link href="{{asset('css/estilos.css')}}" rel="stylesheet">
                                    <!-- Styles -->
                                    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
                                    </link>
                                </link>
                            </link>
                        </link>
                    </link>
                </meta>
            </meta>
        </meta>
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    <a class="navbar-brand" href="#">
                        <img alt="logo" class="imagen1" src="/images/seq2.png"/>
                    </a>
                    <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
                        <span class="navbar-toggler-icon">
                        </span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">
                                    Iniciar Sesión
                                </a>
                            </li>
                            @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">
                                    Registro
                                </a>
                            </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarDropdown" role="button" v-pre="">
                                    {{ Auth::user()->name }}
                                    <span class="caret">
                                    </span>
                                </a>
                                <div aria-labelledby="navbarDropdown" class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Cerrar Sesión
                                    </a>
                                    <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
            @auth
            <!--  // Si el usuario esta identificado... -->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">
                    IDEPMSN
                </a>
                <button aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarNavDropdown" data-toggle="collapse" type="button">
                    <span class="navbar-toggler-icon">
                    </span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('home')}}">
                                Inicio
                                <span class="sr-only">
                                    (current)
                                </span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarDropdownMenuLink">
                                Trámites
                            </a>
                            <div aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
                                <a class="dropdown-item" href="">
                                    Escuela Superior
                                </a>
                                <a class="dropdown-item" href="#">
                                    Escuela Media Superior
                                </a>
                                <a class="dropdown-item" href="#">
                                    Escuela Normal
                                </a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                Seguimiento
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            @endauth
            <main class="py-4">
                @yield('content')
            </main>
        </div>
        <div class="espaciado">
        </div>
        
        <footer>
       
        <div class="container-footer-all">
         
             <div class="container-body">
 
                 <div class="colum1">
                     <h1>TRÁMITE NCORPORACIÓN DE ESCUELAS PARTICULARES</h1>
 
                     <p>OTORGAR RECONOCIMIENTOS DE VALIDEZ OFICIAL DE ESTUDIOS A ESCUELAS PARTICULARES PARA IMPARTIR EDUCACIÓN MEDIA SUPERIOR, SUPERIOR Y NORMALES.</p>
 
                 </div>
 
                 <div class="colum2">
 
                     <h1>Redes Sociales</h1>
 
                     <div class="row">
                    <a href="https://www.facebook.com/educacionqr/"><img src="{{asset('icon/facebook.png')}}"></a> 
                    <a href="https://www.facebook.com/educacionqr/"><label>Siguenos en Facebook</label></a>
                     </div>
                     <div class="row">
                        <a href="https://twitter.com/educacionqr/"><img src="{{asset('icon/twitter.png')}}"></a> 
                      <a href="https://twitter.com/educacionqr/"><label>Siguenos en Twitter</label></a> 
                     </div>
                 </div>
 
                 <div class="colum3">
 
                     <h1>Información de contacto</h1>
 
                     <div class="row2">
                         <img src="{{asset('icon/house.png')}}">
                         <label>Av. Insurgentes No. 600 - Col. Gonzalo Guerrero Teléfono : 01 (983) 83 - 507 - 70 C.P. 77020 - Chetumal, Quintana Roo</label>
                     </div>

                     <div class="row2">
                        <img src="{{asset('icon/smartphone.png')}}">
                        <label>(983) 83 5 07 70 </label>
                    </div>
                 </div>
 
             </div>
         
         </div>
         
         <div class="container-footer">
                <div class="footer">
                     <div class="copyright">
                         © 2020 Todos los Derechos Reservados | <a href="https://qroo.gob.mx/seq">SEQ</a>
                     </div>
 
                     <div class="information">
                         <a href="">Informacion Compañia</a> | <a href="">Privacion y Politica</a> | <a href="">Terminos y Condiciones</a>
                     </div>
                 </div>
 
             </div>
         
     </footer>
     
    </body>
</html>
